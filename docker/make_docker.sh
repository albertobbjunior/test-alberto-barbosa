#!/bin/bash
color1='\033[1;31m'  
color2='\033[0;34m'  
color3='\033[1;32m'  
export KAFKA_DATA=${PWD%/*}
echo -e "${color1}To stop and to remove all Container created by This project"
docker-compose -f $KAFKA_DATA/docker/docker-compose.yml down 

echo -e "${color2}To create and start all Container created by This project"
docker-compose -f $KAFKA_DATA/docker/docker-compose.yml up -d


