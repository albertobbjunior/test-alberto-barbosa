package org.casetest.bi.etl_task_01
/**
* *
 * Alberto Silva Code
 * Data Engineer Test
 * Package will move data from csv to hdfs
 * Disclaimer:conf.csv has parameters that willl be used to execute process.
* *
* */


import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, DataFrame, Dataset, SparkSession}
import org.casetest.bi.io.MappingCsvParameters
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.{to_date, to_timestamp}
import org.casetest.bi.database.DataBaseHive
import org.casetest.bi.dataset.{CustomSchemaSparkToHive, driver, timesheet, truckEventTextPartition}
object RunningSparkCSVToHive {

  def runETL(location_csv:String,hadoopAddres:String): Unit = {
    //*** conform contracts
    val schema  = new CustomSchemaSparkToHive
    val spark:SparkSession = SparkSession.builder()
      .master("local[1]").appName("Project")
      .getOrCreate()

    import spark.implicits._
    /**
     *  @dataset Driver - Dataset that will send to HDFS
     */
    val drivers = spark.read
      .option("header", "true")
      .schema(schema.schemaDriver)
      .option("charset", "UTF8")
      .option("delimiter",",")
      .csv(location_csv+"/drivers.csv")
      .withColumnRenamed("certified_boolean","certified")
      .as[driver]
      .cache()





        /**
         *  @dataset Timesheet - Dataset that will send to HDFS
         */
        val timesheet = spark.read
          .option("header", "true")
          .schema(schema.schemaTimesheet)
          .option("charset", "UTF8")
          .option("delimiter",",")
          .csv(location_csv+"/timesheet.csv")
          .withColumn("hours_logged_float", col("hours_logged").cast(FloatType))
          .withColumn("miles_logge_float", col("miles_logge").cast(FloatType))
          .drop("hours_logged")
          .drop("miles_logge")
          .withColumnRenamed("hours_logged_float","hours_logged")
          .withColumnRenamed("miles_logge_float","miles_logge")
          .as[timesheet]
          .cache()



        /**
         *  @dataset ruckEventTextPartition - Dataset that will send to HDFS
         */
        val truckEventTextPartition = spark.read
          .option("header", "true")
          .schema(schema.schemaTruckEventTextPartition)
          .option("charset", "UTF8")
          .option("delimiter",",")
          .csv(location_csv+"/truck_event_text_partition.csv")
          .withColumn("longitude_float", col("longitude").cast(FloatType))
          .withColumn("latitude_float", col("latitude").cast(FloatType))
          .withColumn("eventDate_formatDate", to_timestamp(to_date($"eventDate", "yyyy-MM-dd")))
          .withColumn("timestampEventDate", to_timestamp(concat($"eventDate",lit(":"),$"eventTime"),"yyyy-MM-dd-HH:mm:ss.SSS"))
          .drop("longitude")
          .drop("latitude")
          .drop("eventDate")
          .withColumnRenamed("longitude_float","longitude")
          .withColumnRenamed("latitude_float","latitude")
          .withColumnRenamed("eventDate_formatDate","eventDate")
          .repartition(col("eventDate"))
          .as[truckEventTextPartition]
          .cache()



        /**
         *  @dataframe driver_kpis - Dataset that will send to HDFS
         */
        val driver_kpis  = timesheet.join(drivers,drivers("driverId") ===  timesheet("driverId") ,"left")
                                     .groupBy(drivers("driverId"),drivers("name"))
                                    .sum("hours_logged","miles_logge")
                                    .withColumnRenamed("sum(hours_logged)","total_hours_logged")
                                     .withColumnRenamed("sum(miles_logge)","total_hmiles_logged")
                                     .cache()

        /**
        *  @dataBaseHive - To persist database into hivedriver_kpis
          */
        val dataBaseHive = new DataBaseHive
        dataBaseHive.persistData(drivers,"overwrite","drivers",hadoopAddres,null)
        dataBaseHive.persistData(timesheet,"overwrite","timesheet",hadoopAddres,null)
        dataBaseHive.persistData(truckEventTextPartition,"overwrite","truckEventTextPartition",hadoopAddres,null)
        dataBaseHive.persistData(driver_kpis,"overwrite","driverskpis",hadoopAddres,null)

  }


  def main(args: Array[String]): Unit = {
    val conf = new MappingCsvParameters("/parameters/conf.csv")
    runETL( conf.configuration("pathcsv"),conf.configuration("hive"))

  }
}
