package org.casetest.bi.dataset
/**
 * *
 * Alberto Silva Code
 * Data Engineer Test
 * Class with schema and classes class that will support SparkDataset
 * *
 * */

import org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp

import java.sql.Date
import org.apache.spark.sql.types.{BooleanType, FloatType, IntegerType, StringType, StructField, StructType};


/**
 *
 *  @Class dangerous_driver case class to DataSet
 */
case class driversData(     eventId:Int,
                            driverId: Int,
                            driverName:String,
                            eventTime:Timestamp,
                            eventType:String,
                            latitudeColumn:Float,
                            longitudeColumn:Float,
                            routeId:Int,
                            routeName:String,
                            truckId: Int,
                            eventDate:Date
                            )


class CustomSchemaSparkToHBase() {


  /**
   * @return a dangerous_driver schema CSV with correct csv type dangerous_driver.csv
   */
   def schemaDrivers=
    StructType(Seq(
      StructField("eventId", IntegerType, true),
      StructField("driverId", IntegerType, true),
      StructField("driverName", StringType, true),
      StructField("eventTime", StringType, true),
      StructField("eventType", StringType, true),
      StructField("latitudeColumn", StringType, true),
      StructField("longitudeColumn", StringType, true),
      StructField("routeId", IntegerType, true),
      StructField("routeName", StringType, true),
      StructField("truckId", IntegerType, true)
      )
    )

  /** Defines a Catalog to for the schema mapping Based on this catalog information will be feed in a key Partited by driver and route.*/
  def dangerous_drive_catalog (cf01:String,cf02:String)  = s"""{
                                          |"table":{"namespace":"default", "name":"dangerous_driver"},
                                          |"rowkey":"key",
                                          |"columns":{
                                          |"key":{"cf":"rowkey", "col":"key", "type":"string"},
                                          |"eventId":{"cf":"${cf01}", "col":"eventId", "type":"int"},
                                          |"driverId":{"cf":"${cf01}", "col":"driverId", "type":"int"},
                                          |"driverName":{"cf":"${cf01}", "col":"driverName", "type":"string"},
                                          |"eventTime":{"cf":"${cf01}", "col":"eventTime", "type":"string"},
                                          |"eventType":{"cf":"${cf01}", "col":"eventType", "type":"string"},
                                          |"routeId":{"cf":"${cf02}", "col":"routeId", "type":"int"},
                                          |"routeName":{"cf":"${cf02}", "col":"routeName", "type":"string"},
                                          |"truckId":{"cf":"${cf02}", "col":"truckId", "type":"int"},
                                          |"latitudeColumn":{"cf":"${cf02}", "col":"latitudeColumn", "type":"double"},
                                          |"longitudeColumn":{"cf":"${cf02}", "col":"latitudeColumn", "type":"double"},
                                          |"eventDate":{"cf":"${cf02}", "col":"eventDate", "type":"double"}
                                          |}
                                          |}""".stripMargin

  /** Defines a Catalog to for the schema mapping Based on this catalog information will be feed in a key Partition and driver and route.*/

  def extra_drivers (cf01:String,cf02:String)  = s"""{
                                                              |"table":{"namespace":"default", "name":"dangerous_driver"},
                                                              |"rowkey":"key",
                                                              |"columns":{
                                                              |"key":{"cf":"rowkey", "col":"key", "type":"string"},
                                                              |"eventId":{"cf":"${cf01}", "col":"eventId", "type":"int"},
                                                              |"driverId":{"cf":"${cf01}", "col":"driverId", "type":"int"},
                                                              |"driverName":{"cf":"${cf01}", "col":"driverName", "type":"string"},
                                                              |"eventTime":{"cf":"${cf01}", "col":"eventTime", "type":"string"},
                                                              |"eventType":{"cf":"${cf01}", "col":"eventType", "type":"string"},
                                                              |"routeId":{"cf":"${cf02}", "col":"routeId", "type":"int"},
                                                              |"routeName":{"cf":"${cf02}", "col":"routeName", "type":"string"},
                                                              |"truckId":{"cf":"${cf02}", "col":"truckId", "type":"int"},
                                                              |"latitudeColumn":{"cf":"${cf02}", "col":"latitudeColumn", "type":"double"},
                                                              |"longitudeColumn":{"cf":"${cf02}", "col":"latitudeColumn", "type":"double"},
                                                              |"eventDate":{"cf":"${cf02}", "col":"eventDate", "type":"double"}
                                                              |}
                                                              |}""".stripMargin




}
