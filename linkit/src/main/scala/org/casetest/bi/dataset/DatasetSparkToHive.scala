package org.casetest.bi.dataset
/**
 * *
 * Alberto Silva Code
 * Data Engineer Test
 * Class with schema and classes class that will support SparkDataset
 * *
 * */

import org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp

import java.sql.Date

import org.apache.spark.sql.types.{BooleanType, FloatType, IntegerType, StringType, StructField, StructType};

case class driver(driverid: Int, name: String,ssn:Int,location:String,certified:Boolean,wage_plan:String)

case class timesheet(driverId: Int, week: Int,hours_logged:Float,miles_logge:Float)

case class truckEventTextPartition(driverId: Int,
                                      truckId: Int,
                                      eventTime:String,
                                      longitude:Float,
                                      latitude:Float,
                                      eventKey:String,
                                      CorrelationId:String,
                                      driverName:String,
                                      routeId:Int,
                                      routeName:String,
                                      eventDate:Timestamp,
                                      timestampEventDate:Timestamp)


/**
 *
 *  @Class customSchema class with CSV schema, will insert null if schema wont be achived
 */

class CustomSchemaSparkToHive() {


  /**
   * @return a TimeSheet schema CSV with correct csv type TimeSheet.csv
   */
  def schemaTimesheet =
    StructType(Seq(
      StructField("driverid", IntegerType, true),
      StructField("week", IntegerType, true),
      StructField("hours_logged", StringType, true),
      StructField("miles_logge", StringType, true)
      )
    )

  /**
   * @return a Drivers schema CSV with correct csv type TimeSheet.csv
   */
  def schemaDriver =
    StructType(Seq(
      StructField("driverId", IntegerType, true),
      StructField("name", StringType, true),
      StructField("ssn", IntegerType, true),
      StructField("location", StringType, true),
      StructField("certified", StringType, false),
      StructField("wage_plan", StringType, false)
      )
    )

  /**
   * @return a TruckEventTextPartition schema CSV with correct csv type TruckEventTextPartition.csv
   */
  def schemaTruckEventTextPartition =
    StructType(Seq(
      StructField("driverId", IntegerType, true),
      StructField("truckId", IntegerType, true),
      StructField("eventTime", StringType, true),
      StructField("eventType", StringType, true),
      StructField("longitude", StringType, true),
      StructField("latitude", StringType, false),
      StructField("eventKey", StringType, false),
      StructField("CorrelationId", StringType, false),
      StructField("driverName", StringType, false),
      StructField("routeId", IntegerType, false),
      StructField("routeName", StringType, false),
      StructField("eventDate", StringType, false)

    )
    )

  def schemaDriverStreaming =
    StructType(Seq(
      StructField("driverId", StringType, true),
      StructField("hours", StringType, true),
      StructField("hoursLogged", StringType, true),
      StructField("milesLogged", StringType, true)
    )
    )
}

