package org.casetest.bi.io

import scala.io.Source

class MappingCsvParameters(file:String) {

  protected val inputFile = s"src/main/resources$file"


  /**
   *
   * @return returns the content on the resources file as an InputStream
   */
  protected def input: String = {
    val resource = new java.io.File(inputFile).exists
    if (resource == false) sys.error(s"Please put the file $inputFile on the resources folder")
    else inputFile
  }

  /**
   *
   * @return Return configuration Address parameters
   */
  def configuration: Map[String,String] = {
    Source.fromFile(input)
      .getLines()
      .drop(1) //skip the header
      .map(x => x)
      .toList
      .map(text => text.split("=")).map(a => (a(0) -> a(1))).toMap
  }


}
