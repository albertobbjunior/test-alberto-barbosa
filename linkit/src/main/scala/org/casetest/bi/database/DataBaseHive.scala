package org.casetest.bi.database


import org.apache.spark.sql.{DataFrame, Dataset}
import org.casetest.bi.dataset.driver
case class DataBaseHive() extends Database {

  override def persistData[T](dataset:Dataset[T],saveType:String,tableName:String,linkServer:String,defaulKeytName:String) : Unit = {
    dataset.write.mode(saveType).parquet( linkServer+tableName)

  }
  override def returnData(dataset:DataFrame) : Unit = {
  }

}
