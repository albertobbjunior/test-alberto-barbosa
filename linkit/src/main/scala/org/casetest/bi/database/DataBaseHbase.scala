package org.casetest.bi.database

import org.apache.hadoop.hbase.io.compress.Compression.Algorithm
import org.apache.hadoop.hbase.{HBaseConfiguration, HColumnDescriptor, HTableDescriptor, TableName}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory}
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog

import org.apache.log4j.Logger

import scala.util.Try

case class DataBaseHbase() extends Database {

  val logger = Logger.getLogger(this.getClass.getName)

  override def persistData[T](dataset:Dataset[T],saveType:String,tableName:String,linkServer:String,defaulKeytName:String) : Unit = {
    logger.info("*** Insert Data into Table ***")
    if (hasDatasetColumn(dataset, "rowkey")) {
      print(tableName)
     // logger.info("*** WRITING INTO HBASE ***")
      dataset.write
        .options(Map(HBaseTableCatalog.tableCatalog -> tableName, HBaseTableCatalog.newTable -> "5"))
        .format("org.apache.spark.sql.execution.datasources.hbase").save()

    } else {
      print("*** Error Insert Data into Table ***")
     }



  }
  def returnData(sparkSession: SparkSession,catalog: String):DataFrame ={
    sparkSession.read.options(Map(HBaseTableCatalog.tableCatalog -> catalog, HBaseTableCatalog.newTable -> "5"))
    .format("org.apache.spark.sql.execution.datasources.hbase")
    .load()
  }

   def updateDataSetRouteName(dataFrame:DataFrame): DataFrame ={
    dataFrame.withColumn("routeName", when(lower(col("routeName"))
      .equalTo("Santa Clara to San Diego".toLowerCase), lit("Los Angeles to Santa Clara")))

  }

  def getConnected(HBASE_CONNECTION:String): Connection = {
    val config = HBaseConfiguration.create()
    config.set("hbase.master", HBASE_CONNECTION)
    config.setInt("timeout", 180000)
    ConnectionFactory.createConnection(config)
  }

  def createDatabaseTable(HBASE_CONNECTION:String,tableName:String,cfName:String):Unit = {
    val conn = getConnected(HBASE_CONNECTION)
    val admin = conn.getAdmin

    val table = new HTableDescriptor(TableName.valueOf(tableName))
    cfName.split(":").toList.map(x => table.addFamily(new HColumnDescriptor(x).setCompressionType(Algorithm.NONE)))

    if (admin.tableExists(table.getTableName)) {
      logger.warn("The Table [" + table.getTableName.getNameAsString + "] was created before.")
    } else {
      try {
        println("Table will be created... ")
        admin.createTable(table)
        println("The Table [" + table.getTableName.getNameAsString + "] was created before. ")
      } catch {

        case e: Exception => e.printStackTrace()

      } finally {
        println("The Table [" + table.getTableName.getNameAsString + "] had error ")
        conn.close()

      }

    }
  }



  def dataFrameWithRowKey[T](dataset:Dataset[T], firstColumn: String, secColumn: String, thirdColumn:String,forthColumn:String): DataFrame = {
      dataset.withColumn("rowkey", concat(col(firstColumn), lit("|"), col(secColumn),lit("|"),col(thirdColumn),lit("|"),col(forthColumn)))
    }
    def hasDatasetColumn[T](dataset:Dataset[T], path: String) = Try(dataset(path)).isSuccess
}
