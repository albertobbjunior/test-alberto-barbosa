package org.casetest.bi.database

import org.apache.spark.sql.{Column, DataFrame, Dataset, SparkSession}
trait Database {

   def persistData[T](dataset:Dataset[T],saveType:String,tableName:String,linkServer:String,defaulKeytName:String) : Unit {}
   def returnData(dataset:DataFrame) : Unit = {}
   def createDatabaseTable() : Unit = {}



}
