package org.casetest.bi.etl_task_03


/**
 * *
 * Alberto Silva Code
 * Data Engineer Test
 * Package will move data from kafka  to hdfs using sparkstreaming
 * Disclaimer:conf.csv has parameters that willl be used to execute process.
 * *
 * */
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.from_json

import org.casetest.bi.dataset.CustomSchemaSparkToHive
import org.casetest.bi.io.MappingCsvParameters


object RunninSparkKafkaToHDFS {

  def runETL(myMap:Map[String,String]): Unit = {

    /**  set variables before start process*/
    val hdfs_location =   myMap("hdfs_server")
    val kafka_server =    myMap("kafka_server")


    /** spark application */
   val spark = SparkSession.builder()
      .appName("Streaming Kafka")
      .master("local[3]").appName("Project")
      .getOrCreate()

    import spark.implicits._

    /** return kafka data dataframe */
    val df = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafka_server)
      .option("subscribe", "linkit")
      .option("startingOffsets", "latest")
      .load()


    /** drivers schema*/
    val schema  = new CustomSchemaSparkToHive

    /** convert kafka data in sql data*/
    val drivers = df.selectExpr("CAST(value AS STRING)", "topic", "offset")
      .withColumn("linkit", (from_json($"value", schema.schemaDriverStreaming) ))
      .selectExpr("topic", "offset",
        "linkit.driverid",
        "linkit.hours",
        "linkit.hoursLogged",
        "linkit.milesLogged"
      )

    drivers.printSchema()

    /** convert kafka data in sql data*/
    val write = drivers.writeStream
      .format("json")
      .outputMode("append")
     // .option("failOnDataLoss", "false")
      .option("path", hdfs_location + "streaming/timesheet")
      .option("checkpointLocation", hdfs_location + "checkpoint")
      .start()
    write.awaitTermination()
  }


  def main(args: Array[String]): Unit = {
    val conf = new MappingCsvParameters("/parameters/conf.csv")
    val myMap = conf.configuration
    runETL(myMap)

  }


}
