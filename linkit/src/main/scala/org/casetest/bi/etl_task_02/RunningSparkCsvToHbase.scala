package org.casetest.bi.etl_task_02
/**
 * *
 * Alberto Silva Code
 * Data Engineer Test
 * Package will move data from csv to hbase
 * Disclaimer:conf.csv has parameters that willl be used to execute process.
 * *
 * */

import org.apache.spark.sql.{Dataset, SparkSession}
import org.casetest.bi.io.MappingCsvParameters
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions.{to_date, to_timestamp}
import org.casetest.bi.database.{DataBaseHbase, DataBaseHive}
import org.casetest.bi.dataset.{CustomSchemaSparkToHBase, driversData}
import org.apache.spark.sql.functions._

object RunningSparkCsvToHbase {

  def runETL(map_parameters:Map[String,String]): Unit = {
    val csv_location      =  map_parameters("pathcsv")
    val database_location =  map_parameters("hive")

    val dataBaseHbase = new DataBaseHbase()

    dataBaseHbase.createDatabaseTable(map_parameters("hbConnection"),"dangerous_driver",map_parameters("cfName"))

    //*** conform contracts

    val spark = SparkSession
                .builder()
               .master("local")
                .getOrCreate()

    /*** @List of CodFamily*/
    val codFamily:List[String] = map_parameters("cfName").split(":").toList

    val schema = new CustomSchemaSparkToHBase

    /*** @return a dangerous_driver schema CSV with correct csv type dangerous_driver.csv*/
    val  dangerousDriver_csv = returnDataSetDriver(spark,csv_location,"/dangerous-driver.csv",schema)

    /*** Format Dataset with new rowkey with rowkeid*/
    val dangerousDriver_key_dd = dataBaseHbase.dataFrameWithRowKey( dangerousDriver_csv, "driverId", "truckID", "eventID","eventTime")
    dangerousDriver_key_dd.printSchema()
    dangerousDriver_key_dd.show()
    /*** Persist Data on HBase*/
    dataBaseHbase.persistData(dangerousDriver_key_dd,null,schema.dangerous_drive_catalog(codFamily(0),codFamily(1)),null,map_parameters("defaulKeyName"))

    /*** @return a extra_driver schema CSV with correct csv type extra_driver.csv*/
    val  extra_driver_csv = returnDataSetDriver(spark,csv_location,"/extra-driver.csv",schema)

    val extra_driver_key_dd = dataBaseHbase.dataFrameWithRowKey( extra_driver_csv, "driverId", "truckID", "eventID","eventTime")
    dataBaseHbase.persistData(extra_driver_key_dd,null,schema.dangerous_drive_catalog(codFamily(0),codFamily(1)),map_parameters("defaulKeytName"),null)

    val dfDangerousDriveDf = dataBaseHbase.
                              returnData(spark,"dangerous_driver")
                                                                         .where(
                                                                                col("rowkeyid") === map_parameters("driverID") + "|" +
                                                                                                             map_parameters("eventID") + "|"  +
                                                                                                             map_parameters("eventID") + "|"  +
                                                                                                              map_parameters("timestamp") + "|"
                                                                          )

     val dfDangerousDriveUpdateDF= dfDangerousDriveDf.withColumn("routeName",
                                                                    when(lower(col("routeName"))
                                                                    .equalTo("Santa Clara to San Diego".toLowerCase), lit("Los Angeles to Santa Clara")))

    dataBaseHbase.persistData(dfDangerousDriveUpdateDF,null,schema.dangerous_drive_catalog(codFamily(0),codFamily(1)),map_parameters("defaulKeytName"),null)

  }

  def main(args: Array[String]): Unit = {
    val conf = new MappingCsvParameters("/parameters/conf.csv")
    //val configuration = conf.configuration.map(x => x.split("=").t
    val myMap = conf.configuration
    runETL(myMap)

  }

   def returnDataSetDriver[T](sparkSession: SparkSession, csv_location:String,csvFileName:String,schema:CustomSchemaSparkToHBase):Dataset[driversData]   = {
     import sparkSession.implicits._
     sparkSession.read
       .option("header", "true")
       .schema(schema.schemaDrivers)
       .option("charset", "UTF8")
       .option("delimiter",",")
       .csv(csv_location+csvFileName)
       .withColumn("longitude_float", col("longitudeColumn").cast(FloatType))
       .withColumn("latitude_float", col("latitudeColumn").cast(FloatType))
       .withColumn("eventDate_formatDate", to_date(col("eventTime"), "yyyy-MM-dd HH:mm:ss.SSS"))
       .withColumn("eventTime", to_timestamp(col("eventTime"),"yyyy-MM-dd HH:mm:ss.SSS"))
       .drop("longitudeColumn")
       .drop("latitudeColumn")
       .drop("eventDate")
       .withColumnRenamed("longitude_float","longitudeColumn")
       .withColumnRenamed("latitude_float","latitudeColumn")
       .withColumnRenamed("eventDate_formatDate","eventDate")
       .repartition(col("eventDate"))
       .cache()
       .as[driversData]
   }
}
