name := "Kafka"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.5"


val hbaseVersion  = "1.1.12"


resolvers += "Hortonworks Repository" at "https://repo.hortonworks.com/content/repositories/releases/"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % scalaVersion.value,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
  "org.apache.hbase"  %  "hbase-common"    % hbaseVersion,
  "org.apache.hbase"  %  "hbase-server"    % hbaseVersion,
  "org.apache.hbase"  %  "hbase-protocol"  % hbaseVersion,
  "org.apache.hbase"  %  "hbase-client"    % hbaseVersion

)

  libraryDependencies++= Seq("com.hortonworks" % "shc-core" % "1.1.1-1.6-s_2.10")


dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.7"
