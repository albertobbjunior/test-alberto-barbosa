CREATE EXTERNAL TABLE drivers (
driverid  int,
name string,
ssn int,
location string,
certified boolean,
wage_plan string
) STORED AS PARQUET
LOCATION '/user/hdfs/drivers';
