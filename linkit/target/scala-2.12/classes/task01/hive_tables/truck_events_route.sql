CREATE EXTERNAL TABLE truckEventTextPartition (
driverId  int,
truckId  Int,
eventTime string,
longitude float,
latitude float,
eventKey string,
CorrelationId string,
driverName string,
routeId int,
routeName string,
eventDate Timestamp,
timestampEventDate Timestamp
)
STORED AS PARQUET
LOCATION '/user/hdfs/truckEventTextPartition'