CREATE EXTERNAL TABLE drivers_kpis (
driverid  int,
name string,
total_hours_logged double,
total_hmiles_logged double
) STORED AS PARQUET
LOCATION '/user/hdfs/driverkpis';
