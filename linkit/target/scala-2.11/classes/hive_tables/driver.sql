CREATE EXTERNAL TABLE drivers (
driverid  int,
name string,
ssn int,
location string,
certified string,
wage_plan string
) STORED AS PARQUET
LOCATION '/user/hdfs/drivers';
