CREATE EXTERNAL TABLE timesheet (
driverId  int,
week  Int,
hours_logged Float,
miles_logged Float
)
STORED AS PARQUET
LOCATION '/user/hdfs/timesheet'
